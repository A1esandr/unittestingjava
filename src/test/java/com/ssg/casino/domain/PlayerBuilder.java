package com.ssg.casino.domain;

import static org.junit.Assert.assertEquals;

public class PlayerBuilder {
    private Player player;
    PlayerBuilder() {
        this.player = new Player();
    }
    public PlayerBuilder joinGame(RollDiceGame game) throws CasinoGameException {
        this.player.joins(game);
        return this;
    }
    public PlayerBuilder buy(int chips) throws CasinoGameException {
        this.player.buy(chips);
        return this;
    }
    public PlayerBuilder bet(Bet bet){
        try {
            this.player.bet(bet);
        } catch (Exception e) {}
        return this;
    }
    public Player build() {
        return player;
    }
}
