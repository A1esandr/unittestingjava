package com.ssg.casino.domain;

public class RollDiceGameBuilder {
    private RollDiceGame rollDiceGame;
    RollDiceGameBuilder() {
        this.rollDiceGame = new RollDiceGame();
    }

    public RollDiceGame build() {
        return rollDiceGame;
    }
}
