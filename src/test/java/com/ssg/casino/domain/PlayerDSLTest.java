package com.ssg.casino.domain;

import org.junit.Test;

import java.util.HashMap;

import static com.ssg.casino.domain.BuilderFactory.playerBuilder;
import static org.junit.Assert.*;

public class PlayerDSLTest {
    @Test
    public void testPlayer_joinsGame() throws CasinoGameException {
        // arrange
        RollDiceGame rollDiceGame = new RollDiceGame();
        Player player = playerBuilder().joinGame(rollDiceGame).build();

        //assert
        assertEquals(rollDiceGame, player.getActiveGame());
    }

    @Test(expected = CasinoGameException.class)
    public void testPlayer_CannotJoinTwice() throws CasinoGameException {
        RollDiceGame game1 = new RollDiceGame();
        RollDiceGame game2 = new RollDiceGame();

        playerBuilder().joinGame(game1).joinGame(game2).build();
    }

    @Test
    public void testRollDiceGame_join6PlayersOnly() throws CasinoGameException {
        RollDiceGame game = new RollDiceGame();
        for (int i = 0; i < RollDiceGame.MAX_PLAYERS; i++) {
            playerBuilder()
                    .joinGame(game)
                    .buy(1)
                    .bet(new Bet(1,1))
                    .build();
        }
        assertFalse(game.canJoin());
    }

    @Test
    public void testPlayer_canBuyChips() throws CasinoGameException {
        RollDiceGame game = new RollDiceGame();
        Player player = playerBuilder()
                .joinGame(game)
                .buy(1)
                .build();

        assertEquals(1, player.getAvailableChips());
    }

    @Test(expected = CasinoGameException.class)
    public void testPlayer_canNotBuyNegativeChips() throws CasinoGameException {
        RollDiceGame game = new RollDiceGame();
        playerBuilder()
                .joinGame(game)
                .buy(-1)
                .build();
    }

    @Test
    public void testPlayer_canBetInGame() throws CasinoGameException {
        RollDiceGame game = new RollDiceGame();
        Player player = playerBuilder()
                .joinGame(game)
                .buy(1)
                .bet(new Bet(1, 1))
                .build();

        assertEquals(1, game.getPlayersBets().size());
        assertTrue(game.getPlayersBets().keySet().contains(player));
    }
}
