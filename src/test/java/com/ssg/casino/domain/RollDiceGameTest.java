package com.ssg.casino.domain;

import junit.framework.TestCase;
import org.junit.Test;

import java.util.HashMap;

import static org.mockito.Mockito.*;

public class RollDiceGameTest extends TestCase {

    class GameStub extends RollDiceGame {
        @Override
        int getWinningScore() {
            return 1;
        }
    }

    public void testAddBet() throws CasinoGameException {
        RollDiceGame rollDiceGame = new RollDiceGame();
        Player testPlayer = new Player();
        Bet testBet = new Bet(1, 1);
        rollDiceGame.addBet(testPlayer, testBet);
        HashMap<Player, Bet> playersBets = rollDiceGame.getPlayersBets();
        assertEquals(1, playersBets.size());

    }

    public void testCanJoin() {
        RollDiceGame game = new RollDiceGame();
        assertTrue(game.canJoin());
    }

    public void testCanNotJoin() throws CasinoGameException {
        RollDiceGame game = new RollDiceGame();
        for (int i = 0; i < RollDiceGame.MAX_PLAYERS; i++) {
            game.addBet(new Player(),new Bet(1,1));
        }
        assertFalse(game.canJoin());
    }

    @Test
    public void testWhenPlay_WinSixBet() throws CasinoGameException {
        RollDiceGame game = new GameStub();
        Player player = new Player();
        player.joins(game);
        game.addBet(player, new Bet(1,1));

        game.play();

        assertEquals(6, player.getChips());
    }

    @Test
    public void testWhenPlay_Lose() throws CasinoGameException {
        RollDiceGame game = new GameStub();
        Player player = new Player();
        player.buy(3);
        player.joins(game);
        game.addBet(player, new Bet(1,2));

        game.play();

        assertEquals(3, player.getChips());
    }

    @Test
    public void testWhenPlay_WinCall() throws CasinoGameException {
        RollDiceGame game = new GameStub();
        Player player = mock(Player.class);
        Bet bet = new Bet(1,1);
        doCallRealMethod().when(player).buy(1);
        doCallRealMethod().when(player).joins(game);
        doCallRealMethod().when(player).bet(bet);
        player.buy(1);
        player.joins(game);
        player.bet(bet);

        game.play();

        verify(player, times(1)).win(6);
    }

    @Test
    public void testWhenPlay_LoseCall() throws CasinoGameException {
        RollDiceGame game = new GameStub();
        Player player = mock(Player.class);
        Player playerTwo = mock(Player.class);
        Bet bet = new Bet(1,2);
        doCallRealMethod().when(player).buy(1);
        doCallRealMethod().when(player).joins(game);
        doCallRealMethod().when(player).bet(bet);
        player.buy(1);
        player.joins(game);
        player.bet(bet);
        Bet betTwo = new Bet(1,3);
        doCallRealMethod().when(playerTwo).buy(1);
        doCallRealMethod().when(playerTwo).joins(game);
        doCallRealMethod().when(playerTwo).bet(betTwo);
        playerTwo.buy(1);
        playerTwo.joins(game);
        playerTwo.bet(betTwo);

        game.play();

        verify(player, times(1)).lose();
        verify(playerTwo, times(1)).lose();
    }

    @Test
    public void testWhenPlay_WinCallAll() throws CasinoGameException {
        RollDiceGame game = new GameStub();
        Player player = mock(Player.class);
        Player playerTwo = mock(Player.class);
        Bet bet = new Bet(1,1);
        doCallRealMethod().when(player).buy(1);
        doCallRealMethod().when(player).joins(game);
        doCallRealMethod().when(player).bet(bet);
        player.buy(1);
        player.joins(game);
        player.bet(bet);
        Bet betTwo = new Bet(1,1);
        doCallRealMethod().when(playerTwo).buy(1);
        doCallRealMethod().when(playerTwo).joins(game);
        doCallRealMethod().when(playerTwo).bet(betTwo);
        playerTwo.buy(1);
        playerTwo.joins(game);
        playerTwo.bet(betTwo);

        game.play();

        verify(player, times(1)).win(6);
        verify(playerTwo, times(1)).win(6);
    }

    @Test
    public void testWhenPlay_WinSixBetAll() throws CasinoGameException {
        RollDiceGame game = new GameStub();
        Player player = new Player();
        Player playerTwo = new Player();
        Bet bet = new Bet(1,1);
        player.buy(1);
        player.joins(game);
        player.bet(bet);
        Bet betTwo = new Bet(1,1);
        playerTwo.buy(1);
        playerTwo.joins(game);
        playerTwo.bet(betTwo);

        game.play();

        assertEquals(6, player.getChips());
        assertEquals(6, playerTwo.getChips());
    }

}