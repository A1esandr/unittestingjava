package com.ssg.casino.domain;

import org.junit.Test;

import static org.junit.Assert.*;

public class PlayerTest {
    @Test
    public void test() {
        assertEquals(1, 1);
    }

    @Test
    public void testBuy() {
        Player player = new Player();
        try {
            player.buy(1);
            assertEquals(1, player.getChips());
        } catch(CasinoGameException e) {
            assertEquals(e.getMessage(), "Buying negative numbers is not allowed");
        }
    }

    @Test
    public void exceptionOnNegativeChips() {
        Player player = new Player();
        try {
            player.buy(-1);
        } catch(CasinoGameException e) {
            assertEquals(e.getMessage(), "Buying negative numbers is not allowed");
        }
    }

    @Test
    public void joinsGame_ActiveGameIsNotEmpty() {
        Player player = new Player();
        RollDiceGame game = new RollDiceGame();

        try {
            player.joins(game);
            assertEquals(game, player.activeGame());
        } catch (CasinoGameException e) {
            assertEquals(e.getMessage(), "Player must leave the current game before joining another game");
        }
    }

    @Test
    public void joinsSecondGame_CannotJoinTwice() {
        Player player = new Player();
        RollDiceGame game1 = new RollDiceGame();
        RollDiceGame game2 = new RollDiceGame();

        try {
            player.joins(game1);
            player.joins(game2);
        } catch (CasinoGameException e) {
            assertEquals(e.getMessage(), "Player must leave the current game before joining another game");
            assertEquals(game1 ,player.activeGame());
        }
    }
}