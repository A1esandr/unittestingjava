package com.ssg.casino.domain;

import java.util.HashMap;
import java.util.Random;

public class RollDiceGame {
    static final int MAX_PLAYERS = 6;
    private HashMap<Player, Bet> playersBets = new HashMap<Player, Bet>();
    public void addBet(Player player, Bet bet) throws CasinoGameException {
        if(! canJoin()) {
            throw new CasinoGameException("There are no seats");
        }
        playersBets.put(player, bet);
    }

    int getWinningScore() {
        Random random = new Random();
        return random.nextInt(6) + 1;
    }

    public void play() throws CasinoGameException {
        int winningScore = getWinningScore();

        for (Player player : playersBets.keySet()) {
            Bet bet = playersBets.get(player);
            if (bet.getScore() == winningScore) {
                player.win(bet.getAmount() * 6);
            } else {
                player.lose();
            }
        }
        playersBets.clear();
    }

    public void leave(Player player) throws CasinoGameException {
        if (!playersBets.containsKey(player)) {
            return;
        }

        playersBets.remove(player);
    }

    public HashMap<Player, Bet> getPlayersBets() {
        return playersBets;
    }

    public boolean canJoin() {
        return playersBets.keySet().size() < 6;
    }
}
